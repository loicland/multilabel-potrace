/*=============================================================================
 * Hugo Raguet 2020
 *===========================================================================*/
#include <cmath> // for sqrt
#include "multilabel_potrace.hpp"
#include "omp_num_threads.hpp"
#include "potrace.hpp"

#define TPL template <typename comp_t, typename int_coor_t, \
    typename real_coor_t>
#define TPLS TPL template <typename Segtype>
#define MPT Multi_potrace<comp_t, int_coor_t, real_coor_t>
#define tMPT typename Multi_potrace<comp_t, int_coor_t, real_coor_t>

#define REALLOC(ptr, type, size) \
    ptr = (type*) realloc_check(ptr, sizeof(type)*((size_t) size));
#define MALLOC(ptr, type, size) \
    ptr = (type*) malloc_check(sizeof(type)*((size_t) size));

using namespace std; 

TPL MPT::Multi_potrace(const comp_t* comp_assign, int_coor_t width,
    int_coor_t height, comp_t number_of_components)
    : comp_assign(comp_assign), width(width), height(height), 
      number_of_components(number_of_components)
{
    raw_contours = nullptr;
    simplified_contours = nullptr;
    smoothed_contours = nullptr;
    bboxes = nullptr;
    pix_on_border = nullptr;
    // smoothing = 1.0;
    straight_line_tol = 1.0;
    smoothing = 0.0; /* TODO: implement smoothing */
    curve_fusion_tol = 0.2;
}

TPL MPT::~Multi_potrace()
{
    free_contours(raw_contours);
    free_contours(simplified_contours);
    free_contours(smoothed_contours);
    free(pix_on_border);
}

TPL void MPT::set_potrace_param(real_coor_t straight_line_tol,
    real_coor_t smoothing, real_coor_t curve_fusion_tol)
{
    if (straight_line_tol < 1.0){ straight_line_tol = 1.0; }
    this->straight_line_tol = straight_line_tol;
    if (smoothing < 0.0){ smoothing = 0.0; }
    if (smoothing >= 4/3.0){ smoothing = 4/3.0; }
    this->smoothing = smoothing;
    if (curve_fusion_tol < 0.0){ curve_fusion_tol = 0.0; }
    this->curve_fusion_tol = curve_fusion_tol;
}

/*-----------------------------------------------------------------------------
 * Step 1: decompose the borders of each components into paths which are either
 * closed, or connecting "triple points", which are points where three or more
 * components are joining
-----------------------------------------------------------------------------*/

/* convert (x, y) coordinate into column-major linear indexing */
TPL size_t MPT::coor_to_index(int_coor_t x, int_coor_t y) const
{ return (size_t) (x + 1)*height - (y + 1); }

TPL size_t MPT::ul(size_t index) const
{ return index - height; }

TPL size_t MPT::ur(size_t index) const
{ return index; }

TPL size_t MPT::dl(size_t index) const
{ return index - height + 1; }

TPL size_t MPT::dr(size_t index) const
{ return index + 1; }

TPL tMPT::Direction MPT::turn_right(Direction d) const
{ return d == UP ? RIGHT : d == RIGHT ? DOWN : d == DOWN ? LEFT : UP; }

TPL tMPT::Direction MPT::turn_left(Direction d) const
{ return d == UP ? LEFT : d == LEFT ? DOWN : d == DOWN ? RIGHT : UP; }

TPL void MPT::compute_bounding_boxes()
{
    MALLOC(bboxes, Bbox, number_of_components);
    for (comp_t comp = 0; comp < number_of_components; comp++){
        bboxes[comp].lower_left.x = numeric_limits<int_coor_t>::max();;
        bboxes[comp].lower_left.y = numeric_limits<int_coor_t>::max();;
        bboxes[comp].upper_right.x = numeric_limits<int_coor_t>::min();;
        bboxes[comp].upper_right.y = numeric_limits<int_coor_t>::min();;
    }
    for (int_coor_t x = 0; x < width; x++){
        size_t index = coor_to_index(x, 0); 
        for (int_coor_t y = 0; y < height; y++){
            comp_t comp = comp_assign[index];
            if (x < bboxes[comp].lower_left.x){
                bboxes[comp].lower_left.x = x;
            }
            if (y < bboxes[comp].lower_left.y){
                bboxes[comp].lower_left.y = y;
            }
            if (x + 1 > bboxes[comp].upper_right.x){
                bboxes[comp].upper_right.x = x + 1;
            }
            if (y + 1 > bboxes[comp].upper_right.y){
                bboxes[comp].upper_right.y = y + 1;
            }
            index -= 1; // column-major indexing, y is incremented
        }
    }
}

TPL tMPT::Direction MPT::find_next_border(comp_t comp, Point_i& p) const
{
    const Bbox& bbox = bboxes[comp];

    for (; p.y < bbox.upper_right.y; p.y++){
        size_t index = coor_to_index(p.x, p.y);
        for (; p.x < bbox.upper_right.x; p.x++){
            if (comp_assign[ur(index)] == comp){
                if ((p.y == 0 || comp_assign[dr(index)] != comp) &&
                    !has_positive(pix_on_border[ur(index)])){
                    return RIGHT;
                }
            }else if (p.x > 0 && p.y > 0){
                if (comp_assign[ul(index)] == comp &&
                    !has_negative(pix_on_border[ul(index)])){
                    return UP;
                }
            }
            index += height; // column-major indexing, x is incremented
        }
        p.x = bbox.lower_left.x;
    }
    return DOWN;
}

TPL bool MPT::compute_raw_path(Point_i& p, Direction& d, const Point_i& p0,
    Path<Point_i>& path, const Border_status border_sign)
{
    size_t index = coor_to_index(p.x, p.y);

    /* determine the components on both sides of the path */
    comp_t comp;
    if (d == UP){
        comp = comp_assign[ul(index)];
        path.other_comp = p.x == width ? out_comp() : comp_assign[ur(index)];
    }else if (d == DOWN){
        comp = comp_assign[dr(index)];
        path.other_comp = p.x == 0 ? out_comp() : comp_assign[dl(index)];
    }else if (d == LEFT){
        comp = comp_assign[dl(index)];
        path.other_comp = p.y == height ? out_comp() : comp_assign[ul(index)];
    }else{ // d == RIGHT
        comp = comp_assign[ur(index)];
        path.other_comp = p.y == 0 ? out_comp() : comp_assign[dr(index)];
    }

    path.length = 0;
    int bufsize = comp < path.other_comp ? 16 : 1;
    MALLOC(path.segments, Point_i, bufsize);

    if (comp >= path.other_comp){ path.segments[0] = p; }

    bool is_triple_point = false;

    while (!is_triple_point){ 
        /* add point to path */
        if (comp < path.other_comp){ // only comp with lowest id records path
            if (path.length == bufsize) {
                bufsize += bufsize/2 + 1;
                REALLOC(path.segments, Point_i, bufsize);
            }
            path.segments[path.length++] = p;
        }

        /* flag the corresponding pixel as visited;
         * only on directions that might trigger a new path */
        if (d == RIGHT && p.x < width){
            set_positive(pix_on_border[ur(index)]);
        }else if (d == UP && p.y > 0){
            set_negative(pix_on_border[ul(index)]);
        }

        /* move to next point and compute next direction */
        comp_t on_left, on_right;
        if (d == UP){ // x != 0
            p.y++;
            index--;
            if (p.y == height){
                on_left = on_right = out_comp();
            }else{
                on_left = comp_assign[ul(index)];
                on_right = p.x == width ? out_comp() : comp_assign[ur(index)];
            }
        }else if (d == DOWN){ // x != width
            p.y--;
            index++;
            if (p.y == 0){
                on_left = on_right = out_comp();
            }else{
                on_left = comp_assign[dr(index)];
                on_right = p.x == 0 ? out_comp() : comp_assign[dl(index)];
            }
        }else if (d == LEFT){ // y != 0
            p.x--;
            index -= height;
            if (p.x == 0){
                on_left = on_right = out_comp();
            }else{
                on_left = comp_assign[dl(index)];
                on_right = p.y == height ? out_comp() : comp_assign[ul(index)];
            }
        }else{ // d == RIGHT // y != height
            p.x++;
            index += height;
            if (p.x == width){
                on_left = on_right = out_comp();
            }else{
                on_left = comp_assign[ur(index)];
                on_right = p.y == 0 ? out_comp() : comp_assign[dr(index)];
            }
        }

        /* determine next direction and check for triple point */
        if (on_left == comp){
            if (on_right == comp){ d = turn_right(d); }
            else if (on_right != path.other_comp){ is_triple_point = true; }
        }else{
            if (on_left != path.other_comp){ is_triple_point = true; }
            if (on_right == comp){ /* connected by diagonal */
                /* not actually a triple point but must be treated as such
                 * otherwise one might not be able to retrieve the path from
                 * the other component (not coming from the end point) */
                is_triple_point = true;
                d = border_sign == POSITIVE ? turn_right(d) : turn_left(d);
            }else{
                d = turn_left(d);
                if (on_right != path.other_comp){ is_triple_point = true; }
            }
        }

        /* check if border is closed */
        if (p == p0) { break; }
    }

    if (comp < path.other_comp && bufsize > path.length){
        REALLOC(path.segments, Point_i, path.length);
    }

    return is_triple_point;
}

TPL tMPT::template Border<tMPT::Point_i>
MPT::compute_raw_border(Point_i p, Direction d)
{
    Border<Point_i> border;
    int& number_of_paths = border.number_of_paths;
    Path<Point_i>*& paths = border.paths;

    number_of_paths = 0;
    int bufsize = 16;
    MALLOC(paths, Path<Point_i>, bufsize);

    Point_i p0 = p;

    Border_status border_sign = d == RIGHT ? POSITIVE : NEGATIVE;

    bool is_triple_point;

    do {
        if (number_of_paths == bufsize){
            bufsize += bufsize/2 + 1;
            REALLOC(paths, Path<Point_i>, bufsize);
        }
        is_triple_point = compute_raw_path(p, d, p0, paths[number_of_paths++],
            border_sign);
    } while (p != p0);

    /* if the border contains more than one path and the origin was not a
     * triple point, the last path is in fact the begining of the first path */
    if (number_of_paths > 1 && !is_triple_point){
        number_of_paths--;
        Path<Point_i>& first_path = paths[0];
        Path<Point_i>& last_path = paths[number_of_paths];
        if (first_path.length){ /* append first path to the last */
            REALLOC(last_path.segments, Point_i,
                last_path.length + first_path.length);
            for (int i = 0; i < first_path.length; i++){
                last_path.segments[last_path.length + i] =
                    first_path.segments[i];
            }
        }
        /* swap first path with last path and update length */
        free(first_path.segments);
        first_path.segments = last_path.segments;
        first_path.length += last_path.length;
    }

    if (bufsize > number_of_paths){
        REALLOC(paths, Path<Point_i>, number_of_paths);
    }
 
    return border;
}

TPL tMPT::template Contour<tMPT::Point_i>
MPT::compute_raw_contour(comp_t comp)
{
    Contour<Point_i> contour;
    int& number_of_borders = contour.number_of_borders;
    Border<Point_i>*& borders = contour.borders;

    number_of_borders = 0;
    int bufsize = 16;
    MALLOC(borders, Border<Point_i>, bufsize);

    /* start at lower left corner of bounding box */
    Point_i p = bboxes[comp].lower_left;
    Direction d = find_next_border(comp, p);
    while (d != DOWN){
        if (number_of_borders == bufsize){
            bufsize += bufsize/2 + 1;
            REALLOC(borders, Border<Point_i>, bufsize);
        }
        borders[number_of_borders++] = compute_raw_border(p, d);
        d = find_next_border(comp, p);
    }

    if (bufsize > number_of_borders){
        REALLOC(borders, Border<Point_i>, number_of_borders);
        /* safety: components identifiers in comp_assign may be nonsequential;
         * in that case, borders of nonexisting components must be set to null
         * we do it explicitely for dumb compilers such as MSVC */
        if (!number_of_borders){ borders = nullptr; }
    }

    return contour;
}

TPL void MPT::compute_raw_contours()
{
    compute_bounding_boxes();

    /* rough estimation of the total length of paths
     * useful for estimating the parallel work load */
    double avg_area = (double) height*width/number_of_components;
    /* area ~= ratio*radius^2 and perim ~= 2*ratio*radius, where ratio is pi
     * for a disk, 4 for a square (considering radius is half side length);
     * we use ratio = 3.5 in perim ~= 2*ratio*sqrt(area/ratio) */
    double avg_perimeter = 7.*sqrt(avg_area/3.5); 
    double total_length = avg_perimeter*number_of_components/2;
    int n_thrds = compute_num_threads(total_length, number_of_components);

    /* boolean map indicating the pixels which have been visited */
    MALLOC(pix_on_border, Border_status, height*width);
    for (size_t index = 0; index < (size_t) height*width; index++){
        pix_on_border[index] = NONE;
    }

    free_contours(raw_contours);
    MALLOC(raw_contours, Contour<Point_i>, number_of_components);

    /* compute contours in parallel */
    #pragma omp parallel for schedule(dynamic) num_threads(n_thrds)
    /* unsigned loop counter is allowed since OpenMP 3.0 (2008)
     * but MSVC compiler still does not support it as of 2020 */
    for (long comp = 0; comp < (long) number_of_components; comp++){
        raw_contours[comp] = compute_raw_contour(comp);
    }

    free(pix_on_border); pix_on_border = nullptr;
    free(bboxes); bboxes = nullptr;
}

/*-----------------------------------------------------------------------------
 * Step 2: approximate each of these paths into sequences of straight lines or
 * simple third order Bézier curves using an adaptation of "potrace" software
 * by Peter Selinger to possibly nonclosed paths with fixed end points.
 *---------------------------------------------------------------------------*/

/* assume raw_contours have been computed */
TPL void MPT::trace_path(comp_t comp, int border_num, int path_num)
{
    /* get borders and paths */
    const Border<Point_i>& raw_border = raw_contours[comp].borders[border_num];
    const Path<Point_i>& raw_path = raw_border.paths[path_num];

    if (!raw_path.length){ /* path recorded and processed by "other comp" */
        if (simplified_contours){
            Path<Point_r>& simplified_path =
                simplified_contours[comp].borders[border_num].paths[path_num];
            simplified_path.length = 0;
            MALLOC(simplified_path.segments, Point_r, 1);
            simplified_path.segments[0] = raw_path.segments[0];
            simplified_path.other_comp = raw_path.other_comp;
        }
        if (smoothed_contours){
            Path<Curve>& smoothed_path =
                smoothed_contours[comp].borders[border_num].paths[path_num];
            smoothed_path.length = 0;
            MALLOC(smoothed_path.segments, Curve, 1);
            smoothed_path.segments[0] = Curve(raw_path.segments[0]);
            smoothed_path.other_comp = raw_path.other_comp;
        }
        return;
    }

    /* convert to the potrace path structure */
    potrace::privpath_t* potrace_path = potrace::privpath_new();
    const Point_i& endpoint = raw_border.endpoint(path_num);
    bool is_cycle = raw_path.startpoint() == endpoint;
    potrace_path->len = is_cycle ? raw_path.length : raw_path.length + 1;
    MALLOC(potrace_path->pt, potrace::point_t, potrace_path->len);
    for (int i = 0; i < raw_path.length; i++){
        potrace_path->pt[i].x = raw_path.segments[i].x;
        potrace_path->pt[i].y = raw_path.segments[i].y;
    }
    if (!is_cycle){
        potrace_path->pt[raw_path.length].x = endpoint.x;
        potrace_path->pt[raw_path.length].y = endpoint.y;
    }

    /* trace the path with potrace */
    process_path(potrace_path, smoothing, curve_fusion_tol, straight_line_tol,
        is_cycle);

    /* copy results into multilabel potrace data structure */
        /* TODO: smooth and use fcurve */
    potrace::privcurve_t* potrace_curve = &potrace_path->curve;
    if (simplified_contours){
        Path<Point_r>& simplified_path =
            simplified_contours[comp].borders[border_num].paths[path_num];
        simplified_path.length = potrace_path->m;
        MALLOC(simplified_path.segments, Point_r, simplified_path.length);
        for (int i = 0; i < simplified_path.length; i++){
            // simplified_path.segments[i].x = potrace_path->pt[po[i]].x;
            // simplified_path.segments[i].y = potrace_path->pt[po[i]].y;
            simplified_path.segments[i].x = potrace_curve->vertex[i].x;
            simplified_path.segments[i].y = potrace_curve->vertex[i].y;
        }
        simplified_path.other_comp = raw_path.other_comp;
    }
    if (smoothed_contours){
        std::cerr << "Multilabel Potrace: smoothing not implemented yet."
            << std::endl;
        exit(EXIT_FAILURE);
    }

    /* free potrace path structure */
    potrace::privpath_free(potrace_path);
}

TPL void MPT::trace(bool keep_intermediate_stages)
{
    compute_raw_contours();
    
    /* get the total length of paths for estimating the parallel work load
     * based on a quadratic cost of tracing algorithm */
    long int total_square_length = 0;
    for (comp_t comp = 0; comp < number_of_components; comp++){
        const Contour<Point_i>& contour = raw_contours[comp];
        for (int i = 0; i < contour.number_of_borders; i++){
            const Border<Point_i>& border = contour.borders[i];
            for (int j = 0; j < border.number_of_paths; j++){
                const Path<Point_i>& path = border.paths[j];
                total_square_length += path.length*path.length;
            }
        }
    }
    int n_thrds = compute_num_threads(total_square_length,
        number_of_components);

    /* allocate the contours array only for requested information */
    if (!smoothing || keep_intermediate_stages){
        free_contours(simplified_contours);
        MALLOC(simplified_contours, Contour<Point_r>, number_of_components);
    }
    if (smoothing){
        free_contours(smoothed_contours);
        MALLOC(smoothed_contours, Contour<Curve>, number_of_components);
    }

    /* trace contours in parallel */
    #pragma omp parallel for schedule(dynamic) num_threads(n_thrds)
    /* unsigned loop counter is allowed since OpenMP 3.0 (2008)
     * but MSVC compiler still does not support it as of 2020 */
    for (long comp = 0; comp < (long) number_of_components; comp++){
        Contour<Point_i>& contour = raw_contours[comp];
        if (simplified_contours){
            simplified_contours[comp].number_of_borders =
                contour.number_of_borders;
            MALLOC(simplified_contours[comp].borders,
                Border<Point_r>, contour.number_of_borders);
        }
        if (smoothed_contours){
            smoothed_contours[comp].number_of_borders =
                contour.number_of_borders;
            MALLOC(smoothed_contours[comp].borders,
                Border<Curve>, contour.number_of_borders);
        }
        for (int i = 0; i < contour.number_of_borders; i++){
            Border<Point_i>& border = contour.borders[i];
            if (simplified_contours){
                simplified_contours[comp].borders[i].number_of_paths =
                    border.number_of_paths;
                MALLOC(simplified_contours[comp].borders[i].paths,
                    Path<Point_r>, border.number_of_paths);
            }
            if (smoothed_contours){
                smoothed_contours[comp].borders[i].number_of_paths =
                    border.number_of_paths;
                MALLOC(smoothed_contours[comp].borders[i].paths,
                    Path<Curve>, border.number_of_paths);
            }
            for (int j = 0; j < border.number_of_paths; j++){
                trace_path(comp, i, j);
                /* cannot free, might be needed of another path's endpoint */
            }
            if (!keep_intermediate_stages){ free_border(border); }
        }
        if (!keep_intermediate_stages){ free_contour(contour); }
    }
    if (!keep_intermediate_stages){ free_contours(raw_contours); }
}

/**  instantiate for compilation  **/
template class Multi_potrace<uint16_t, uint16_t, float>;
template class Multi_potrace<uint16_t, uint16_t, double>;
