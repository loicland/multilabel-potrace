import numpy as np
import sys, os
import matplotlib.lines as mlines # compat. with Python 2
from matplotlib import cm
import matplotlib.pyplot as plt

sys.path.append(os.path.join(os.path.realpath(os.path.dirname(__file__)), 
                                              "./bin"))

from multilabel_potrace_shp import multilabel_potrace_shp, Shape

def create_shape(wid, hei):
    """
    create a simple test array
    """
    # comp_assign = np.asfortranarray(np.zeros((wid, hei), dtype='uint16'))
    # test with C-contiguous array
    comp_assign = np.array(np.zeros((wid, hei), dtype='uint16'))
    x, y = np.ogrid[:wid, :hei]
    x, y = x/float(wid), y/float(hei) # compat. with Python 2
    # draw a triangle
    comp_assign[x + y <= 0.5] = 1
    # draw a disk on top
    comp_assign[(x - 0.5)**2 + (y - 0.5)**2 <= 0.4**2] = 2
    # cut smaller disks
    # comp_assign[(x - 0.25)**2 + (y - 0.25)**2 <= 0.1**2] = 0
    # comp_assign[(x - 0.6)**2 + (y - 0.6)**2 <= 0.1**2] = 0
    # add smaller disks (nonconnected components are not well rendered by fill)
    comp_assign[(x - 0.25)**2 + (y - 0.25)**2 <= 0.1**2] = 3
    comp_assign[(x - 0.6)**2 + (y - 0.6)**2 <= 0.1**2] = 4
    return comp_assign

wid, hei = 100, 80
comp_assign = create_shape(wid, hei)

shp_polygons = multilabel_potrace_shp(comp_assign, straight_line_tol = 1.0)

fig = plt.figure(1)
fig.clf()

cmap = cm.get_cmap('viridis', comp_assign.max() + 1)

ax = fig.add_subplot(121)
ax.imshow(comp_assign, extent = (0.0, hei, wid, 0.0), cmap = cmap)
ax.set_aspect('equal')
ax.axis('off')
ax.set_title('raster')

ax = fig.add_subplot(122)
for poly, c in zip(shp_polygons, cmap.colors):
    ax.fill(*poly.points.tolist(), c = c)
ax.set_aspect('equal')
ax.set_xlim(0, hei)
ax.set_ylim(0, wid)
ax.axis('off')
ax.set_title('polygons')

fig.show()
# fig.savefig('multi_potrace_shp_test.png')
