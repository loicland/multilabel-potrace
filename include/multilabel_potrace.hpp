/*=============================================================================
 * Extract and simplifies contours delimiting homogeneous connected components
 * within a 2D grid structure (typically, pixels of an image).
 * 
 * The assignment of each pixel to its component must be given. Then, this
 * software allows to perform the following steps :
 *
 * 1. decompose the borders of each components into paths which are either
 * closed, or connecting "triple points", which are points where three or more
 * components are joining 
 * 2. approximate each of these paths into sequences of straight lines or
 * cubic Bézier curves using an adaptation of the "potrace" software by Peter
 * Selinger, to possibly nonclosed paths with fixed end points.
 *
 * The final components contours can then be reconstructed from the sequence of
 * simplified paths. Keeping the "triple points" fixeFranced ensures that neighboring
 * components agree on their border.
 *
 * Hugo Raguet 2020
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *===========================================================================*/
#pragma once
#include <cstdlib> // for size_t, malloc, exit
#include <iostream>
#include <type_traits> // for conditional typedef
#include <limits>

template <typename comp_t, typename int_coor_t, typename real_coor_t>
class Multi_potrace
{
    /***  data types  ***/
protected:

    /* paths are represented as aggregate of "path segments"; smooth vectorized
     * paths uses Bézier curves approximating corners, while polygons, which
     * are shapes with only straight lines, are optimaly represented directly
     * with vertex points; we thus use a base class with a "vertex" concept */

    struct Segment { const Segment& vertex() const { return *this; } };

    template <typename coor_t>
    struct Point2D : Segment
    {
        coor_t x, y;

        bool operator == (const Point2D<coor_t>& p) const
            { return x == p.x && y == p.y; }
        bool operator != (const Point2D<coor_t>& p) const
            { return x != p.x || y != p.y; }

        template <typename other_coor_t>
        const Point2D<coor_t>& operator = (const Point2D<other_coor_t>& p)
            { x = p.x; y = p.y; return *this; }

        const Point2D<coor_t>& vertex() const { return *this; }
    };

    typedef Point2D<int_coor_t> Point_i;
    typedef Point2D<real_coor_t> Point_r;

    struct Bbox { Point_i lower_left, upper_right; };

    /**  vectorized curves, similar to potrace representation  **/

    struct Curve : Segment
    {
        static Point_r not_a_point()
        {
            Point_r p; p.x = p.y = std::numeric_limits<real_coor_t>::lowest();
            return p;
        }

        Point_r control_points[3];

        const Point_r& vertex() const { return this->control_points[1]; }

        /* only one vertex, used only on triple points */
        Curve(const Point_i& v)
        {
            control_points[0] = not_a_point();
            control_points[1] = v;
            control_points[2] = not_a_point(); 
        }

        /* straight line segment */
        Curve(real_coor_t vx, real_coor_t vy, real_coor_t bx, real_coor_t by)
        {
            control_points[0] = not_a_point();
            control_points[1].x = vx;
            control_points[1].y = vy;
            control_points[2].x = bx;
            control_points[2].y = by;
        }

        /* Bézier curve segment */
        Curve(real_coor_t ux, real_coor_t uy, real_coor_t vx, real_coor_t vy,
            real_coor_t bx, real_coor_t by)
        {
            control_points[0].x = ux;
            control_points[0].y = uy;
            control_points[1].x = vx;
            control_points[1].y = vy;
            control_points[2].x = bx;
            control_points[2].y = by;
        }

        bool is_bezier_curve() const 
        { return control_points[0] != not_a_point(); }
    };

    /* conditional on the type of points within a segment: raw paths use
     * Point_i since coordinates are integers, simplified paths use Point_r
     * since non-triple points can be adjusted with real coordinates; and of
     * course curve use also real coordinates; requires C++11 */
    template <typename Segtype> using Point = typename std::conditional<
        std::is_same<Segtype, Point_i>::value, Point_i, Point_r>::type;

    /**  contours  **/

    /* a path links two triple points (or is a closed path if it is a border
     * involving only two components), and define a frontier between two
     * components; each path being involved in the border of two different
     * components, by convention only the component with lowest id records the
     * path in order to save space; the component with highest id records only
     * the first point and the path length is set to zero;
     * this is templated because the path can be constituted of:
     * points with integer coordinates for raw paths, or
     * points with real coordinates for optimal polygons, or
     * curve segments for smooth paths */

    template <typename Segtype>
    struct Path
    {
        int length;
        Segtype* segments;
        comp_t other_comp; // the other component

        /* get a reference to the start point of a path */
        const Point<Segtype>& startpoint() const
            { return segments[0].vertex(); }
    };

    /* a border is a set of paths defining a closed curve */
    template <typename Segtype>
    struct Border
    {
        int number_of_paths;
        Path<Segtype>* paths;

        /* get a reference to the end point of a path; requires access to next
         * path, so it is a method of the Border class */
        const Point<Segtype>& endpoint(int path_num) const
        {
            return paths[(path_num+1) % number_of_paths].segments[0].vertex();
        }
    };

    /* a contour is a set of borders delimiting a component */
    template <typename Segtype> struct Contour
    {
        int number_of_borders;
        Border<Segtype>* borders;
    };

private:
    enum Direction {UP, DOWN, LEFT, RIGHT};

    /* given a component, borders are iteratively constructed so as to keep the
     * component on its left;
     * thus, if a border encloses the component, we say that it is positive,
     * because if such a border is circular, it is visited in the positive
     * (trigonometric) sense; alternatively, if a border defines a hole in the
     * component, we say that it is negative, because if such a border is
     * circular, it is visited in the negative (clockwise) sense;
     * usually components are connected, so that there should be exactly one
     * positive border (the outer border), and can be any number of negative
     * borders (inner borders); note that current implementation allow for
     * disconnected components (thus with possibly several positive borders);
     * NONE and BOTH are useful for flagging pixels involved in borders */
    enum Border_status : char // requires C++11 to ensure 1 byte
        {NONE, POSITIVE, NEGATIVE, BOTH};

    bool has_positive(const Border_status& border_status) const
    { return border_status == POSITIVE || border_status == BOTH; }
    bool has_negative(const Border_status& border_status) const
    { return border_status == NEGATIVE || border_status == BOTH; }

    void set_positive(Border_status& border_status)
    { border_status = has_negative(border_status) ? BOTH : POSITIVE; }
    void set_negative(Border_status& border_status)
    { border_status = has_positive(border_status) ? BOTH : NEGATIVE; }

    /***  members  ***/

private:
    /* component assignment of each pixel;
     * monodimensional array, column-major format, i.e. pixels are linearly
     * indexed first top-to-bottom then left-to-right */
    const comp_t* comp_assign;

    const int_coor_t width, height;

    /* bounding box of each component */
    Bbox* bboxes;

    /* map indicating points already assigned to a border;
     * status is indicated on pixels rather than on points, enabling work in
     * parallel along components; in coherence with find_next_border(),
     * given a component, a point on a positive border (enclosing it) is
     * flagged on its upper right pixel, while a point on a negative border
     * (hole border) is flagged on its upper left pixel */
    Border_status* pix_on_border;

    /* parameters for simplification with potrace */
    /* fidelity to the raster : how far (in l1 distance, pixel unit) from a
     * path can a straight line approximate it */
    real_coor_t straight_line_tol;
    real_coor_t smoothing; // potrace alphamax parameter
    real_coor_t curve_fusion_tol; // potrace opttolerance parameter

protected:
    const comp_t number_of_components;

    /* array containing the raw contours of each component */
    Contour<Point_i>* raw_contours;

    /* array containing the simplified contours of each component */
    Contour<Point_r>* simplified_contours;

    /* array containing the smoothed contours of each component */
    Contour<Curve>* smoothed_contours;

    /***  methods  ***/
public:
    Multi_potrace(const comp_t* comp_assign, int_coor_t width,
        int_coor_t height, comp_t number_of_components);
    ~Multi_potrace();

    void set_potrace_param(real_coor_t straight_line_tol = 1.0,
        real_coor_t smoothing = 1.0, real_coor_t curve_fusion_tol = 0.2);

    void trace(bool keep_intermediate_stages = false);

private:
    /*-------------------------------------------------------------------------
     * Step 1: decompose the borders of each components into paths 
     *-----------------------------------------------------------------------*/

    /* convert (x, y) coordinate into column-major linear indexing */
    size_t coor_to_index(int_coor_t x, int_coor_t y) const;

    /* The point of coordinate (x, y) is the lower left corner of the pixel of
     * coordinate (x, y); the following convert the linear index of this pixel
     * to the linear index of other surrounding pixels
     *
     * ul  |  ur (this is the pixel of coordinate (x, y))
     * --(x,y)---
     * dl  |  dr
     *
     */
    size_t ul(size_t index) const;
    size_t ur(size_t index) const; 
    size_t dl(size_t index) const; 
    size_t dr(size_t index) const; 

    Direction turn_right(Direction dir) const;
    Direction turn_left(Direction dir) const;

    void compute_bounding_boxes();

    /* find the next point which is at a lower left corner of a border of the
     * given component; the grid is explored left-to-right then bottom-to-top,
     * starting from the given point;
     * path are computed so as to leave the considered component on the left,
     * thus the upper left pixel corresponding to the point lies within the
     * given component if the border is positive (enclosing the component), and
     * outside if it the border is negative (hole border);
     * return the next direction leaving the component on the left, this is
     * either RIGHT for a positive border, or UP for a negative border;
     * return DOWN if no next border is available */
    Direction find_next_border(comp_t comp, Point_i& p) const;

    /* compute a path from the given point in the given direction, leaving the
     * current component to the left of the path, stopping at next triple point
     * or p0; boolean return value indicate if end point is a triple point;
     * the sign of the current border being computed must be given for
     * determining turn at checkerboard pattern */
    bool compute_raw_path(Point_i& p, Direction& d, const Point_i& p0,
        Path<Point_i>& path, const Border_status sign);

    /* compute a raw border, starting at given point with given direction */
    Border<Point_i> compute_raw_border(Point_i p, Direction d);

    /* compute the raw contour of a given component */
    Contour<Point_i> compute_raw_contour(comp_t comp);

    /* compute raw contours of all components, in parallel */
    void compute_raw_contours();

    /*-------------------------------------------------------------------------
     * Step 2: approximate each paths into sequences of lines or Bézier curves
     *-----------------------------------------------------------------------*/

    /* trace a path and place results directly in  */
    void trace_path(comp_t comp, int border_num, int path_num);

protected:
    /* no component can have this identifier */
    static comp_t out_comp() { return std::numeric_limits<comp_t>::max(); }

    /* each path is involved in the border of two different components but
     * only one records the path; beware that the order is reversed when
     * considering the other component: check the other_comp field;
     * WARNING: complexity linear in number of paths involved in other_comp */
    template <typename Segtype>
    const Path<Segtype>& get_path(comp_t comp, int border_num, int path_num,
        Contour<Segtype>* contours) const;

    /**  memory management  **/

private:
    template <typename Segtype> void free_path(Path<Segtype>& path);
    template <typename Segtype> void free_border(Border<Segtype>& border);
    template <typename Segtype> void free_contour(Contour<Segtype>& contour);

protected:
    template <typename Segtype> void free_contours
        (Contour<Segtype>*& contours);

    /* allocate memory and fail with error message if not successful */
    static void* malloc_check(size_t size);

    /* simply free if size is zero */
    static void* realloc_check(void* ptr, size_t size);

    /* some friend functions for displaying or debugging
     * https://en.wikibooks.org/wiki/More_C%2B%2B_Idioms/Making_New_Friends
     * explain why this must be defined within the class declaration */
    template<typename coor_t>
    friend std::ostream& operator << (std::ostream& out,
        const Point2D<coor_t>& point)
    { return out << "(" << point.x << ", " << point.y << ")"; }

    template<typename Segtype>
    friend std::ostream& operator << (std::ostream& out,
        const Path<Segtype>& path)
    {
        out << "[" << path.other_comp << "] " << path.segments[0];
        for (int i = 1; i < path.length; i++){
            out << " -> " << path.segments[i];
        }
        if (!path.length){
            return out << " - - -> ";
        }else{
            return out << " -> ";
        }
    }

    template<typename Segtype>
    friend std::ostream& operator << (std::ostream& out,
        const Border<Segtype>& border)
    {
        for (int i = 0; i < border.number_of_paths; i++){
            out << border.paths[i] << "\n";
        }
        return out;
    }

    template<typename Segtype>
    friend std::ostream& operator << (std::ostream& out,
        const Contour<Segtype>& contour)
    {
        for (int i = 0; i < contour.number_of_borders; i++){
            out << contour.borders[i] << "\n";
        }
        return out;
    }

    friend std::ostream& operator << (std::ostream& out,
        const Multi_potrace<comp_t, int_coor_t, real_coor_t>& mp)
    {
        if (mp.raw_contours){
            out << "\nRaw:\n";
            for (comp_t comp = 0; comp < mp.number_of_components; comp++){
                out << "\n\tcomponent " << comp << "\n\n" <<
                    mp.raw_contours[comp];
            }
        }

        if (mp.simplified_contours){
            out << "\nSimplified:\n";
            for (comp_t comp = 0; comp < mp.number_of_components; comp++){
                out << "\n\tcomponent " << comp << "\n\n" <<
                    mp.simplified_contours[comp];
            }
        }

        return out;
    }
};

/* some definitions must be available outside the class */

#define TPL template <typename comp_t, typename int_coor_t, \
    typename real_coor_t>
#define TPLS TPL template <typename Segtype>
#define MPT Multi_potrace<comp_t, int_coor_t, real_coor_t>
#define tMPT typename Multi_potrace<comp_t, int_coor_t, real_coor_t>

TPLS inline const tMPT::template Path<Segtype>&  MPT::get_path(comp_t comp,
    int border_num, int path_num, Contour<Segtype>* contours) const
{
    const Border<Segtype>& border = contours[comp].borders[border_num];
    const Path<Segtype>& path = border.paths[path_num];
    if (!path.length){
        const Point<Segtype>& endpoint = border.endpoint(path_num);
        const Contour<Segtype>& other_contour = contours[path.other_comp];
        for (int i = 0; i < other_contour.number_of_borders; i++){
            const Border<Segtype>& other_border = other_contour.borders[i];
            for (int j = 0; j < other_border.number_of_paths; j++){
                const Path<Segtype>& other_path = other_border.paths[j];
                if (other_path.other_comp == comp &&
                    other_path.startpoint() == endpoint){
                    return other_path;
                }
            }
        }
    }
    return path;
}

TPLS void MPT::free_path(Path<Segtype>& path)
{
    free(path.segments);
    path.segments = nullptr; // prevent against double freeing
}

TPLS void MPT::free_border(Border<Segtype>& border)
{
    for (int j = 0; j < border.number_of_paths; j++){
        free_path(border.paths[j]);
    }
    free(border.paths);
    /* prevent against double freeing */
    border.number_of_paths = 0;
    border.paths = nullptr;
}

TPLS void MPT::free_contour(Contour<Segtype>& contour)
{
    for (int i = 0; i < contour.number_of_borders; i++){
        free_border(contour.borders[i]);
    }
    free(contour.borders);
    /* prevent against double freeing */
    contour.number_of_borders = 0;
    contour.borders = nullptr;
}

TPLS void MPT::free_contours(Contour<Segtype>*& contours)
{
    if (contours){
        for (comp_t comp = 0; comp < number_of_components; comp++){
            free_contour(contours[comp]);
        }
        free(contours);
        contours = nullptr;
    }
}

TPL void* MPT::malloc_check(size_t size){
    void *ptr = malloc(size);
    if (!ptr){
        std::cerr << "Multilabel potrace: not enough memory." << std::endl;
        exit(EXIT_FAILURE);
    }
    return ptr;
}

/* simply free if size is zero */
TPL void* MPT::realloc_check(void* ptr, size_t size){
    if (!size){
       free(ptr); 
       return nullptr; 
    }
    ptr = realloc(ptr, size);
    if (!ptr){
        std::cerr << "Multilabel potrace: not enough memory." << std::endl;
        exit(EXIT_FAILURE);
    }
    return ptr;
}

#undef TPL
#undef TPLS
#undef MPT
#undef tMPT
