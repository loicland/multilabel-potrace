/*=============================================================================
 * Necessary headers from potrace for calling the actual tracing algorithm.
 *
 * Hugo Raguet 2020
 *===========================================================================*/
#pragma once
namespace potrace {

extern "C" {

#include "potrace/trace.h"

/* trace.h was not intended to be included by the end users: it includes
 * curve.h which includes auxiliary.h, which defines undesirable macros */
#undef sign
#undef abs
#undef min
#undef max
#undef sq
#undef cu

} /* extern C */

} /* namespace potrace */

