addpath('bin/')

%{
width = 100;
height = 100;
[X, Y] = meshgrid(1:width, 1:height);
comp_assign = kmeans([X(:) Y(:)], 6);
comp_assign = uint16(reshape(comp_assign, [width, height]) - 1);
comp_assign(comp_assign == 2) = 5; % test for missing componentse
%}

%{
width = 10;
height = 10;
comp_assign = uint16(zeros(width, height));
comp_assign(2:4, 2:4) = 1;
comp_assign(5:9, 5:9) = 2; % 1 | 2 | 3
comp_assign(6:8, 6:8) = 0;
%}

% %{ % test what happens on diagonally connected components
width = 2;
height = 2;
comp_assign = uint16(zeros(width, height));
comp_assign(1, 1) = 1;
comp_assign(2, 2) = 1;
%}

image([0.5 width - 0.5], [0.5 height - 0.5], flipud(comp_assign));
axis("xy", "square");
colormap(repmat(linspace(0, 1, max(comp_assign(:) + 1))', [1 3]));

multilabel_potrace_mex(comp_assign);
multilabel_potrace_shp_mex(comp_assign);
